# skycoin-node-sync-test

Utility to check Skycoin node's blockchain sync performance.

Node tester will spawn skycoin node with required flags and will wait
for required number of active connections. Once it has enough
connections it will periodically check blockchain sync status and will
record basic sync metrics. If sync progress will stale tester will
notify via stdout and will perform required number of additional
attempts before interrupting testing process. Once testing is finished
node process will be shutdown and testing report along with node's
stdout and stderr will be returned in JSON format.

## Installation

Node tester is a conventional golang utility, regular "go get" will
work on this repository:

```bash
$ go get -v ./...
```

Alternatively you can use provided docker image that will bundle
skycoin node and tester inside golang:latest container:

```bash
docker build -t skycoin-node-sync-test .
```

To run node and tester with default args:

```bash
docker run skycoin-node-sync-test
```

To run with optional flags and custom process:

```bash
docker run skycoin-node-sync-test skycoin-node-sync-test -connections 1 skycoin
```

## Usage

Tester expects one mandatory argument: command of the process to spawn
(normally skycoin). Additional configuration arguments:

* `-apiAddr` - URL of a node to test, defaults to `http://localhost:6420`.
* `-connections` - Number of connections to wait before proceeding with sync testing. Defaults to 3.
* `-timeout` - Number of seconds to wait before marking testing step as a failure. Defaults to 30.
* `-attemtps` - Number of attempts to perform before interrupting testing process. -1 will make tester wait indefinitely. Defaults to -1.
* `-pipeLogs` - If provided node's stdout and stdin will be also piped to the tester's output. Defaults to false.
* `-out` - Location of a report json file. If not provided report will be sent to stdout.

## Report's JSON schema

```golang
type Result struct {
	Success      bool          `json:"success"`       // indicates that test was successful
	Error        error         `json:"error"`         // error that forced to interrupt testing process
	CurrentBlock uint64        `json:"highest_block"` // highest block recorded at the last attempt
	StartedAt    time.Time     `json:"started_at"`    // starting timestamp
	SyncedAt     time.Time     `json:"synced_at"`     // timestamp of the last successful attempt
	Duration     time.Duration `json:"duration"`      // test's duration
	StdOut       string        `json:"stdout"`        // node's stdout
	StdErr       string        `json:"stderr"`        // node's stderr
}
```

Example report:

```json
{
  "success": true,
  "error": null,
  "highest_block": 115,
  "started_at": "2018-09-20T07:43:16.151361877Z",
  "synced_at": "2018-09-20T07:43:19.155152957Z",
  "duration": 3003791271,
  "stdout": "[2018-09-20T07:43:16Z] INFO [file]: Created data directory /root/.skycoin\n...[2018-09-20T07:43:19Z] INFO [main]: Goodbye\n",
  "stderr": ""
}
```

## Skycoin APIs

Tester uses following skycoin node's API:

* [NetworkConnections](https://godoc.org/github.com/skycoin/skycoin/src/api#Client.NetworkConnections) to check number of active connections.
* [BlockchainProgress](https://godoc.org/github.com/skycoin/skycoin/src/api#Client.BlockchainProgress) to check sync progress.
