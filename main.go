package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"time"
)

var (
	apiAddr      = flag.String("apiAddr", "http://localhost:6420", "url of a node api endpoint")
	maxAttempts  = flag.Int("attempts", -1, "number of attempts to perform while waiting for blockchain progress. -1 wait indifinitely.")
	timeout      = flag.Int("timeout", 30, "number of seconds to wait in each step")
	requiredConn = flag.Int("connections", 3, "number of required node connections")
	pipeLogs     = flag.Bool("pipeLogs", false, "pipe node's stdout and stderr to the output")
	resultFile   = flag.String("out", "", "location of a report file. by default will write to stdout")
)

func main() {
	flag.Usage = func() {
		fmt.Fprintln(flag.CommandLine.Output(), "Usage skycoin-node-sync-test [flags] cmd [args]:")
		flag.PrintDefaults()
	}

	flag.Parse()

	stdout, stderr, cmd, err := startProcess()
	if err != nil {
		log.Fatal("Failed to spawn process: ", err)
	}

	stdoutBuf := bytes.NewBuffer(nil)
	stderrBuf := bytes.NewBuffer(nil)
	go func() {
		if _, err := io.Copy(stdoutBuf, stdout); err != nil {
			log.Println("Failed to read from stdout")
		}

		if _, err := io.Copy(stderrBuf, stderr); err != nil {
			log.Println("Failed to read from stderr")
		}
	}()

	checker := NewChecker(*apiAddr, *maxAttempts)
	result := checker.CheckNode(*requiredConn, time.Duration(*timeout)*time.Second)
	cmd.Process.Signal(os.Interrupt)

	if err := cmd.Wait(); err != nil {
		log.Fatalf("Process failure: %s", err)
	}

	result.StdOut = stdoutBuf.String()
	result.StdErr = stderrBuf.String()

	if err := result.Write(*resultFile); err != nil {
		log.Fatal("Failed to write result file:", err)
	}
}

func startProcess() (io.Reader, io.Reader, *exec.Cmd, error) {
	args := flag.Args()
	if len(args) == 0 {
		return nil, nil, nil, fmt.Errorf("empty process arguments")
	}

	cmd := exec.Command(args[0], args[1:]...)
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to pipe to stdout: %s", err)
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to pipe to stderr: %s", err)
	}

	if err := cmd.Start(); err != nil {
		return nil, nil, nil, fmt.Errorf("process %s failed to start: %s", args, err)

	}

	if *pipeLogs {
		return io.TeeReader(stdout, os.Stdout), io.TeeReader(stderr, os.Stderr), cmd, nil
	}

	return stdout, stderr, cmd, nil
}
