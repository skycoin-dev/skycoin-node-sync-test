package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"time"

	"github.com/skycoin/skycoin/src/api"
	"github.com/skycoin/skycoin/src/readable"
)

type APIClient interface {
	NetworkConnections() (*api.Connections, error)
	BlockchainProgress() (*readable.BlockchainProgress, error)
}

type Result struct {
	Success      bool          `json:"success"`       // indicates that test was successful
	Error        error         `json:"error"`         // error that forced to interrupt testing process
	CurrentBlock uint64        `json:"highest_block"` // highest block recorded at the last attempt
	StartedAt    time.Time     `json:"started_at"`    // starting timestamp
	SyncedAt     time.Time     `json:"synced_at"`     // timestamp of the last successful attempt
	Duration     time.Duration `json:"duration"`      // test's duration
	StdOut       string        `json:"stdout"`        // node's stdout
	StdErr       string        `json:"stderr"`        // node's stderr
}

func (r *Result) Write(filepath string) error {
	var resultWriter io.Writer
	if filepath != "" {
		f, err := os.OpenFile(filepath, os.O_RDWR|os.O_CREATE, 0755)
		if err != nil {
			return fmt.Errorf("failed to create file: %s", err)
		}
		defer f.Close()

		resultWriter = f
	} else {
		resultWriter = os.Stdout
	}

	if err := json.NewEncoder(resultWriter).Encode(r); err != nil {
		return fmt.Errorf("failed to encode json result: %s", err)
	}

	return nil
}

type Checker struct {
	client        APIClient
	maxAttempts   int
	checkInterval time.Duration
}

func NewChecker(apiAddr string, maxAttempts int) *Checker {
	client := api.NewClient(apiAddr)
	return &Checker{client, maxAttempts, time.Second}
}

func (c *Checker) CheckNode(requiredConnections int, timeout time.Duration) *Result {
	result := &Result{StartedAt: time.Now(), Success: false}

	log.Println("Checking node...")
	if _, err := c.WaitForConnections(requiredConnections, timeout); err != nil {
		result.Error = fmt.Errorf("failed to reach %d active connections: %s", requiredConnections, err)
		return result
	}

	log.Printf("Got enough active connections: %d\r\n", requiredConnections)
	currentAttempt := 0
	for {
		current, highest, err := c.WaitForBlockChange(result.CurrentBlock, timeout)
		log.Printf("Sync progress %d of %d\r\n", current, highest)
		if err != nil {
			log.Printf("Sync process failure: %s", err)

			currentAttempt += 1
			if c.maxAttempts != -1 && currentAttempt >= c.maxAttempts {
				result.Error = fmt.Errorf("sync process failed: number of attempts exceeded")
				return result
			}

			continue
		}

		result.CurrentBlock = current
		result.SyncedAt = time.Now()

		if current == highest {
			break
		}
	}

	result.Success = true
	result.Duration = time.Now().Sub(result.StartedAt)
	return result
}

func (c *Checker) WaitForConnections(numConn int, timeout time.Duration) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	ticker := time.NewTicker(c.checkInterval)
	defer ticker.Stop()

	connCount := func() (int, error) {
		res, err := c.client.NetworkConnections()
		if err != nil {
			return 0, fmt.Errorf("failed to query network connection: %s", err)
		}

		return len(res.Connections), nil
	}

	for {
		select {
		case <-ticker.C:
			current, err := connCount()
			if err != nil {
				log.Println(err)
			}

			if err != nil || current < numConn {
				continue
			}

			return current, nil
		case <-ctx.Done():
			return 0, ctx.Err()
		}
	}
}

func (c *Checker) WaitForBlockChange(startBlock uint64, timeout time.Duration) (uint64, uint64, error) {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	ticker := time.NewTicker(c.checkInterval)
	defer ticker.Stop()

	blockchainProgress := func() (uint64, uint64, error) {
		res, err := c.client.BlockchainProgress()
		if err != nil {
			return 0, 0, fmt.Errorf("failed to query blockchain progress: %s", err)
		}

		return res.Current, res.Highest, nil
	}

	var current, highest uint64
	var err error
	for {
		select {
		case <-ticker.C:
			current, highest, err = blockchainProgress()
			if err != nil {
				log.Println(err)
			}

			if err != nil || current <= startBlock {
				continue
			}

			return current, highest, nil
		case <-ctx.Done():
			return current, highest, ctx.Err()
		}
	}
}
